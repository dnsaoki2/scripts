import argparse
import configparser
import json
import requests
import json
from urllib.parse import urlencode

from base_log import Log
from base_token import Token

CONFIG_FILE = 'config.ini'


class Base():
    def __init__(self):
        # args config
        self.argparse = argparse.ArgumentParser(description='scripts')
        self.default_args()
        self.extra_args()
        self.log_args()
        self.args = self.argparse.parse_args()

        # read config file
        self.config = configparser.ConfigParser()
        self.config.read(CONFIG_FILE)

        # set log file
        self.log = Log(self.args.silent)

        # set token
        self.token = Token(self.log)
    
    def print(cls, response):
        print(json.dumps(response, indent=4))

    def get_token(cls):
        return cls.token.discovery_token(cls.args.env)

    def default_args(cls):
        cls.argparse.add_argument('env', help='Ambiente')
        cls.argparse.add_argument('tenant_id', help='Tenant Id')

    def log_args(cls):
        cls.argparse.add_argument(
            '--silent',
            '-s',
            dest='silent',
            action="store_true",
            help='Mostrar mensagens de log'
        )

    def extra_args(cls):
        pass

    def print(cls, data):
        print(json.dumps(data, indent=4))

    def headers(cls, method):
        token = cls.get_token()
        json_key = 'Accept' if method in ['get', 'delete'] else 'Content-Type'
        return {
            json_key: 'application/json',
            'Authorization': 'Bearer {}'.format(token) if token else None
        }

    def do(cls, method, url, data=None):
        headers = cls.headers(method)
        response = {
            'get': requests.get(url, headers=headers),
            'post': requests.post(url, headers=headers, data=data),
            'put': requests.put(url, headers=headers, data=data),
            'delete': requests.delete(url, headers=headers),
        }[method]
        return response.json()


class BackstageApisBase(Base):
    def __init__(self, api_name=None):
        self.api_name = api_name
        super().__init__()

    def default_args(cls):
        if not cls.api_name:
            cls.argparse.add_argument('api_name', help='Nome da Api')
        cls.argparse.add_argument('env', help='Ambiente')
        cls.argparse.add_argument('tenant_id', help='Tenant Id')

    def extra_args(cls):
        cls.argparse.add_argument('-d', dest='data', help='Data')
        cls.argparse.add_argument('--page', dest='page', help='Pagina')
        cls.argparse.add_argument(
            '--perPage', dest='per_page', help='Quantida de itens por página')
        cls.argparse.add_argument(
            '--orderBy', dest='order_by', help='Ordenação dos itens')

    def api_url(cls, _id, filter=None):
        _filter = {
            'filter[page]': cls.args.page or 1,
            'filter[perPage]': cls.args.per_page or 10,
            'filter[order]': cls.args.order_by or 'created'
        }
        host = cls.config[cls.args.env]['APIS_URL'].format(
            cls.args.api_name or cls.api_name,
            cls.args.tenant_id
        )
        return '{}/{}?{}'.format(host, _id, urlencode(_filter))

    def do(cls, method, _id='', data=None):
        data = data or cls.args.data
        url = cls.api_url(_id)
        return super(BackstageApisBase, cls).do(method, url, data=data)
