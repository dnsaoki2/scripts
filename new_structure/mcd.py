import sys
import os
from pick import pick
from subprocess import Popen, PIPE, call

WORKSPACE_PATH = "/Users/denis.aoki/workspace"
PERSONAL_PATH = "{}/personal".format(WORKSPACE_PATH)
WORK_PATH = "{}/work/gitlab".format(WORKSPACE_PATH)

dirs = []


def find(path, dir_name):
    task = Popen('ls {} | grep {}'.format(
        path, dir_name), shell=True, stdout=PIPE)
    data = task.stdout.read()
    paths = data.decode('utf-8').split('\n')
    for p in paths:
        if p != '':
            dirs.append('{}/{}'.format(
                path, p
            ))


def ls(path):
    task = Popen('ls {}'.format(path), shell=True, stdout=PIPE)
    data = task.stdout.read()
    paths = data.decode('utf-8').rstrip('\n')
    return ['{}/{}'.format(path, i) for i in paths.split("\n")]


dir_name = sys.argv[1]

# find in personal folder
find(PERSONAL_PATH, dir_name)

# find in work folder
work_paths = ls(WORK_PATH)
for i in work_paths:
    find(i, dir_name)


dirs_min = [i.split(WORKSPACE_PATH)[1] for i in dirs]

for i in dirs:
    print(i)
# option, index = pick(dirs_min, "Selecione o projeto")

# print(dirs[index])
