from termcolor import colored


class Log():
    def __init__(self, log_show):
        self.log_show = log_show

    def message(cls, message):
        if not cls.log_show:
            print(message)
