##
# Script para pegar os dados dos clients das apis não migradas para o APIs V2
##

from base import Base

URL = "http://backstage-migration-monitor-prod.cloud.globoi.com/apis-v2"


class Migrations(Base):
    def get_token(cls):
        pass

    def default_args(cls):
        cls.argparse.add_argument("api_name", help="Nome da Api")
        cls.argparse.add_argument(
            "--filter", "-f", dest="filter", help="Filtro", default="clientID")

    def get(cls):
        response = cls.do("get", "{}/{}".format(URL, cls.args.api_name))
        client_ids = []
        clients = []

        for client in response.get("v1Origins", []):
            client_info = client.get(cls.args.filter, {})
            if client.get("clientID", None) not in client_ids:
                client_ids.append(client.get("clientID", None))
                clients.append({
                    "clientId": client.get("clientID", None),
                    "appName": client.get("appName", None),
                })
        cls.print(clients)


a = Migrations()
a.get()
