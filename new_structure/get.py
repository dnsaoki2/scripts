##
# Script para pegar os dados de uma api
##

from base import BackstageApisBase

api = BackstageApisBase()
response = api.do('get')
api.print(response)