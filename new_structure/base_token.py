import configparser
import os
import requests

CONFIG_FILE = 'config.ini'


class Token():
    def __init__(self, log):
        self.config = configparser.ConfigParser()
        self.config.read(CONFIG_FILE)
        self.log = log

    def from_env(cls):
        return os.environ.get('TOKEN', None)

    def from_app(cls, env):
        base_64 = cls.config[env].get('BASE_64', None)

        if not base_64:
            return None

        cls.log.message("request para accounts")
        url = cls.config[env]['ACCOUNTS_URL']
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic {}'.format(base_64)
        }
        response = requests.post(url,
                                 headers=headers,
                                 data="grant_type=client_credentials"
                                 )
        return response.json().get('access_token', None)

    def discovery_token(cls, env):
        token = cls.from_env() or cls.from_app(env)
        if not token:
            # TODO: add log
            cls.log.message("sem token")
            exit(1)
        return token
