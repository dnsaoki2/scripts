##
# Script para atulizar algum dado
# Uso: python update.py <api_name> <env> <tenant_id> [...args]
# args:
#   data: campos a serem alterados. Ex: '{"using": "true"}'
#   ids: Modificar apenas uma lista de itens (id/id/id)
##

from hierarchy_base import HierarchyBase


class Structure(HierarchyBase):
    def __init__(self):
        super().__init__("backstage-structure")
        self.structure = []

    def get_tree(cls):
        tree = cls.read_tree()
        for category in tree:
            cls.structure.append(category.get('id'))
        return cls.structure


class Commercial(HierarchyBase):
    def __init__(self):
        super().__init__("commercial-api")
        self.commercial = []

    def get_commercial(cls):
        # commercial = cls.read_tree()
        return cls.data

    def save_in_file(cls, _id, category_id):
        with open('./data/commercial-api-{}-{}-ids-deleted'.format(
          cls.env, cls.tenant_id), 'r+') as file:
            message = str({
                "commercial_id": _id,
                "category_id": category_id
            })
            for line in file:
                if message in line:
                    break
            else:
                file.write('{}\n'.format(message))

    def delete_commercial(cls, ids):
        for _id in ids:
            cls.delete(_id)


base_structure = Structure()
base_commercial = Commercial()

category = base_structure.get_tree()
commercial = base_commercial.get_commercial()

not_in = []

for c in commercial:
    if not c.get('category') in category:
        base_commercial.save_in_file(c.get('id'), c.get('category'))
        not_in.append(c.get('id'))

base_commercial.delete_commercial(not_in)
