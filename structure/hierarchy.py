##
# Script para pegar dados da hierarquia através de um id ou do primeiro item
# da estrutura
# Uso: python hierarchy.py <env> <tenant_id>
# args:
#   data: campos a serem alterados. Ex: '{"using": "true"}'
#   id: id do item
##

import json
from base import Base

STRUCTURE_URL = 'https://structure.backstage{}.globoi.com/hierarchy/{}/{}'


class Hierarchy(Base):
    def __init__(self):
        super().__init__("backstage-structure")

    def default_args(cls, api_name):
        cls.parser.add_argument('env', help='Ambiente')
        cls.parser.add_argument('tenant_id', help='Tenant Id')
        cls.parser.add_argument('--id', '-i', help='id')

    def random_id(cls):
      data = cls.get(no_message=True)
      return data.get("items")[0].get("id")

    def get_all(cls):
        url = STRUCTURE_URL.format(cls.env_name, cls.api_name, cls.tenant_id)
        _id = cls.extra_args.id or cls.random_id()
        return cls.get(id=_id, host=url, no_message=True)

base = Hierarchy()
print(json.dumps(base.get_all()))
# print(base.random_id())
