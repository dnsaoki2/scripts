##
# Script para atulizar algum dado
# Uso: python update.py <api_name> <env> <tenant_id> [...args]
# args:
#   data: campos a serem alterados. Ex: '{"using": "true"}'
#   ids: Modificar apenas uma lista de itens (id/id/id)
##

import json
from hierarchy_base import HierarchyBase


class Update(HierarchyBase):
    def extra_args(cls):
        cls.parser.epilog = 'python update.py backstage-structure qa01 g1 -i id1/id2/id3 -d \'{"path": "true"}\'' # noqa
        cls.parser.add_argument('-d', '--data', help='Corpo da mensagem', dest='body', default='{}')
        cls.parser.add_argument('-i', '--ids', help='Modificar apenas o id', dest='ids')

    def get_tree(cls):
        return [
            {"id": _id} for _id in cls.extra_args.ids.split('/')
        ] if cls.extra_args.ids else cls.read_tree()


base = Update()

tree = base.get_tree()

for category in tree:
    base.put(category.get('id'), data=json.loads(base.extra_args.body))
