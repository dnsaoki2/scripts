##
# Script para atulizar algum dado
# Uso: python update.py <api_name> <env> <tenant_id> [...args]
# args:
#   data: campos a serem alterados. Ex: '{"using": "true"}'
#   ids: Modificar apenas uma lista de itens (id/id/id)
##

from base import Base
from termcolor import colored


class FindImports(Base):
    def __init__(self):
        super().__init__("admin-lib-imports")

    def extra_args(cls):
        cls.parser.epilog = 'python imports.py prod globocom'  # noqa
        cls.parser.add_argument(
            '-l', '--lib', help='nome da lib', dest='lib_name', default='')

    def get_all(cls):
        filter = cls.get_filter_all()
        return cls.get(filter=filter)

    def get_lib(cls):
        if(not cls.extra_args.lib_name):
            print(colored("invalid lib_name", "red"))
            return

        data = cls.get_all()
        for i in data['items']:
            if cls.extra_args.lib_name in i.get("imports"):
                print('App: {}'.format(i.get("app")))


base = FindImports()
base.get_lib()
