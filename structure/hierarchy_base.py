from termcolor import colored
from base import Base


class HierarchyBase(Base):
    def __init__(self, api_name=''):
        super().__init__(api_name)
        self.data = []

    def get_all(cls, page=1):
        _filters = cls.get_filter_all(page)
        categories = cls.get(filter=_filters)
        cls.data.extend(categories.get("items", []))

        if 'nextPage' in categories:
            cls.get_all(categories.get('nextPage'))

    def get_categories(cls, parent):
        response = []
        for category in cls.data:
            if category.get('parent') == parent:
                response.append(category)

        return response

    def select_root(cls):
        response = []
        parents = cls.get_categories("")

        print("...Produtos do tenant: {} \n".format(
            [parent.get("name") for parent in parents])
        )

        for parent in parents:
            name = colored(parent['name'], "yellow")
            message = 'Utilizar o produto: {} ? (Y / N)'.format(name)
            print(message)
            if input() in ['Y', 'y', 'yes', 'YES', 'Yes']:
                response.append(parent)

        return response

    def children(cls, data, response):
        for category in data:
            response.append(category)
            children = cls.get_categories(category.get('id'))

            if len(children) != 0:
                cls.children(children, response)

        return response

    def read_tree(cls):
        print(colored("--------------------BAIXANDO AS INFORMAÇÕES--------------------", "red"))
        cls.get_all()
        print(colored("--------------------         PRONTO        --------------------\n \n", "red"))

        if cls.no_tree:
            return cls.data

        print(colored("-------------------- Produtos --------------------", "red"))
        parents = cls.select_root()
        print("...Produtos selecionados: {}".format(
            colored([parent.get("name") for parent in parents], "yellow")
        ))
        print(colored("-------------------- PRONTO --------------------\n \n", "red"))
        return cls.children(parents, [])
