import os
import json
import requests
import argparse

from termcolor import colored


CONF = {
    'dev': {
        'env_name': '.dev',
        'base_64': 'YnJDb2k5NHNYU1hXR1RsVFpyYklqQT09OnJ3aERYZ2J3Y3E2bmg3RE43N29oc1E9PQ=='
    },
    'qa': {
        'env_name': '.qa',
        'base_64': 'SW8zdHIrWW9ZUkFYNW8rMW45VDJodz09Omt6Uy9qa25GdFlHWmMzd1RTaDZOL3c9PQ=='
    },
    'qa01': {
        'env_name': '.qa01',
        'base_64': 'L1AvMGVjclBpWTBRV3A5bnZMVlBoQT09OjdKNnVtdSsyVHVVZVlxSmV6cTNqOEE9PQ=='
    },
    'qa02': {
        'env_name': '.qa02'
    },
    'prod': {
        'env_name': ''
    }
}

APIS_URL = 'https://apis.backstage{}.globoi.com/api/v2/{}/{}'
ACCOUNTS_URL = 'https://accounts.backstage{}.globoi.com/token'


class Base():
    def default_args(cls, api_name):
        if not api_name:
            cls.parser.add_argument('api_name', help='Nome da api')
        cls.parser.add_argument('env', help='Ambiente')
        cls.parser.add_argument('tenant_id', help='Tenant Id')
        cls.parser.add_argument(
            '--no-tree', help='Api não é um árvore', action='store_true', dest='no_tree')

    def extra_args(cls):
        pass

    def __init__(self, api_name=None):
        self.parser = argparse.ArgumentParser(
            description='Scripts para gestão da Estrutura')
        self.default_args(api_name)
        self.extra_args()
        args = self.parser.parse_args()
        self.api_name = api_name or args.api_name
        self.env = args.env
        self.env_name = CONF[self.env]['env_name']
        self.tenant_id = args.tenant_id
        self.hostname = APIS_URL.format(
            CONF[self.env]['env_name'], self.api_name, self.tenant_id)
        self.no_tree = args.no_tree if "no_tree" in args else False
        self.extra_args = args

    def discover_token(cls, no_message=False):
        if CONF[cls.env].get('base_64', None):
            if not no_message:
                print("...... GET TOKEN FROM APP")
            host = ACCOUNTS_URL.format(CONF[cls.env]['env_name'])
            custom_headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic {}'.format(CONF[cls.env].get('base_64', None))
            }
            response = cls.post("grant_type=client_credentials",
                                host=host, no_message=True, custom_headers=custom_headers)

            return response.get('access_token', None)
        return None

    def headers(cls, method, custom_headers=None, no_message=False):
        if custom_headers:
            return custom_headers

        try:
            token = cls.discover_token(no_message) or os.environ['TOKEN']
        except KeyError:
            print(colored('ERR: ---INVALID TOKEN---', 'red'))
            exit(1)

        if method == 'get' or method == 'delete':
            return {
                'Accept': 'application/json',
                'Authorization': 'Bearer {}'.format(token)
            }

        return {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer {}'.format(token)
        }

    @staticmethod
    def handle_response(response, status=200, no_message=False):
        if response.status_code == 401 or response.status_code == 403:
            print(colored('...... ERR: status code: {}'.format(
                response.status_code), 'red'))

        if response.status_code == 204:
            print('...... done')
            return []

        if response.status_code != status and response.status_code != 200:
            print(colored('...... ERR: status code: {}, {}'.format(
                response.status_code, response.json()), 'red'))
            return []

        if not no_message:
            print('...... done')
        return response.json()

    def get_filter_all(cls, page=1):
        filter_tenant = 'filter[where][tenantId]={}'.format(cls.tenant_id)
        filter_order = 'filter[order]=created'
        filter_page = 'filter[perPage]=1000&filter[page]={}'.format(page)

        return '{}&{}&{}'.format(filter_tenant, filter_order, filter_page)

    def get(cls, id='', filter='', host=None, no_message=False, custom_headers=None):
        if not no_message:
            print('... GET FROM {} WITH ID OR FILTER {} IN {}'.format(
                cls.api_name, id or filter, cls.env))

        url = '{}/{}'.format(host or cls.hostname, id)
        if filter:
            url = '{}?{}'.format(host or cls.hostname, filter)

        response = requests.get(
            url, headers=cls.headers('get', custom_headers, no_message))
        return cls.handle_response(response, no_message=no_message)

    def get_versions(cls, id, host=None):
        print('... GET VERSION FROM {} WITH ID {}'.format(cls.api_name, id))
        url = '{}/{}/versions'.format(host or cls.hostname, id)
        response = requests.get(url, headers=cls.headers('get'))

        return cls.handle_response(response)

    def post(cls, data, host=None, no_message=False, custom_headers=None):
        if not no_message:
            print('... CREATE ON {} IN {}'.format(cls.api_name, cls.env))

        response = requests.post(
            url=host or cls.hostname,
            headers=cls.headers('post', custom_headers),
            data=data if isinstance(data, str) else json.dumps(data)
        )

        return cls.handle_response(response, 201, no_message)

    # update
    def put(cls, id, data, host=None):
        print('... UPDATE {} TO {} IN {}'.format(cls.api_name, id, cls.env))

        response = requests.put(
            url='{}/{}'.format(host or cls.hostname, id),
            headers=cls.headers('put'),
            data=json.dumps(data)
        )

        return cls.handle_response(response)

    # delete
    def delete(cls, id, host=None):
        print('... DELETE FROM {} THE ID {} IN {}'.format(
            host.split('/')[-2] if host else cls.api_name, id, cls.env))

        response = requests.delete(
            url='{}/{}'.format(host or cls.hostname, id),
            headers=cls.headers('delete')
        )

        return cls.handle_response(response, 204)
