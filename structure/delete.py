##
# Script para apagar todos os dados de uma api.
# Uso: python delete.py <api_name> <env> <tenantId> [...args]
# args:
#   ids=id/id/id: deletar somente os ids especificados
#   apis=api/api1/api2: apagar o dados das apis plugadas
##

from termcolor import colored
from hierarchy_base import HierarchyBase


class Delete(HierarchyBase):
    def __init__(self, api_name=''):
        super().__init__(api_name)

    def extra_args(cls):
        cls.parser.epilog = 'python delete.py backstage-structure qa01 g1 -i id1/id2/id3 -a commercial-api/styles-api' # noqa
        cls.parser.add_argument('-i', '--ids', help='IDs das categorias a serem deletadas')
        cls.parser.add_argument(
            '-a', '--apis', help='Nome das apis plugadas que se deseja apagar também')

    def get_tree(cls):
        if cls.extra_args.ids:
            return [{"id": _id} for _id in cls.extra_args.ids.split('/')]
        tree = cls.read_tree()

        print('------------------------------------------------------------------------------------') # noqa
        delete_root = input('Deseja apagar as categoria root ? (Y / N)\n') in ['Y', 'Yes', 'YES', 'y'] # noqa
        if not delete_root:
            tree = [category for category in tree if category.get('parent') != ""]

        return tree

    def plugin(cls, category, host):
        _filter = 'filter[where][category]={}'.format(category.get('id'))
        response = base.get(filter=_filter, host=host, no_message=True)

        if response.get('item_count') > 0:
            base.delete(response.get('items')[0].get('id'), host=host)


if __name__ == '__main__':
    base = Delete()
    tree = base.get_tree()

    print(colored('\n Apagando a Estrutura: ', 'yellow'))
    for category in reversed(tree):
        base.delete(category['id'])
    print(colored('Pronto', 'yellow'))

    if base.extra_args.apis:
        plugins = base.extra_args.apis.split('/')
        print(colored('\n Apagando das apis plugins', 'yellow'))
        for plugin in plugins:
            print(colored('...{}'.format(plugin), 'yellow'))
            host = base.hostname.replace(base.api_name, plugin)
            for category in reversed(tree):
                base.plugin(category, host)
            print(colored('pronto', 'yellow'))
