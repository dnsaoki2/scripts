##
# Script para atulizar algum dado
# Uso: python revert.py <api_name> <env> <tenant_id> [...args]
# args:
#   ids: Modificar apenas uma lista de itens (id/id/id)
#   fields: Campos para serem modificados
##


from termcolor import colored

from hierarchy_base import HierarchyBase


class Revert(HierarchyBase):
    def extra_args(cls):
        cls.parser.epilog = 'python revert.py backstage-structure qa01 g1 -i id1/id2/id3 -f field1/field2/field3'  # noqa
        cls.parser.add_argument(
            '-f', '--fields', help='Campos', dest='fields', default=None)
        cls.parser.add_argument(
            '-i', '--ids', help='Modificar apenas o id', dest='ids', required=True)

    def get_ids(cls):
        return cls.extra_args.ids.split('/')

    def get_fields(cls, resource_fields):
        return [field for field in cls.extra_args.fields.split('/')] \
            if cls.extra_args.fields \
            else [field for field in list(resource_fields.keys()) if not cls.ignore_field(field)]

    def get_new_format(cls, version):
        result = {}
        latest_version = version.get("latest_version", {}).get("resource")
        old_version = version.get("old_version", {}).get("resource")
        fields = cls.get_fields(old_version)
        for field in fields:
            if latest_version.get(field, None) != old_version.get(field, None):
                result[field] = old_version.get(field, None)

        return result

    def version(cls, id):
        versions = base.get_versions(id)
        if(versions.get("itemCount", 0) <= 1):
            return {}

        return {
            "latest_version": versions.get("items")[0],
            "old_version": versions.get("items")[1]
        }

    def ignore_field(cls, field_name):
        ignore_fields = ["tenantId", "versionId",
                         "createdBy", "modified", "created", "id"]
        if field_name in ignore_fields:
            return True
        return False

    def print_changes(cls, latest_version, new_version):
        resource = latest_version.get("resource", {})

        print("------ Alterações para {} ------".format(resource.get("id")))
        for field in list(new_version.keys()):
            print("\n" + colored(field, "blue"))
            msg = "... {} -> {}".format(
                colored(resource.get(field), "red"),
                colored(new_version.get(field), "green"),
            )
            print(msg)
        print("------ Done ------")


base = Revert()

ids = base.get_ids()

for _id in ids:
    version = base.version(_id)
    new_data = base.get_new_format(version)
    if len(list(new_data.keys())) == 0:
        continue
    base.print_changes(version.get("latest_version", {}), new_data)

    ask = input("\nDeseja aplicar a modificação?(y/n): ")
    if ask.lower() in ["y", "yes"]:
        base.put(_id, data=new_data)
