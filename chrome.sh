#!/bin/bash

function _open_chrome {
  $(open -n /Applications/Google\ Chrome.app --args --enable-speech-input "$1")  
}

function open_store {
  store_url="http://store.backstage.globoi.com/project"

  store_origin=$(git remote | grep store)
  project_name=""
  if [ $store_origin ]; then
    project_name=$(echo $(git remote get-url store))
  else
    project_name=$(echo $(git remote get-url origin))
  fi
  project_name=$(echo $project_name | cut -d':' -f2 | cut -d'.' -f1)

  _open_chrome ${store_url}/${project_name}
}

function open_gitlab {
  gitlab_url="https://gitlab.globoi.com/"

  origin=$(git remote | grep origin)

  project_name=""
  if [ $origin ]; then
    project_name=$(echo $(git remote get-url origin))
  fi
  project_name=$(echo $project_name | sed -e 's/gitlab@//g' | sed -e 's/:/\//g' | sed -e 's/.git//g')

  _open_chrome ${project_name}
}

function list_commands {
  echo "Uso: "
  echo "  run <command> <...options>"
  echo ""
  echo "command:"
  echo -e "  - store"
  echo -e "  - gitlab"
}

case $1 in
"store") open_store ;;
"gitlab") open_gitlab ;;
*) list_commands ;;
esac