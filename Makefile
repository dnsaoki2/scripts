BIN=/usr/local/bin

setup:
	@pip install -r ./requirements.txt

lint:
	flake8 --max-line-length=101

uninstall:
	rm ${BIN}/script.sh
	rm ${BIN}/chrome.sh
	unlink ${BIN}/run
	unlink ${BIN}/chrome

install: uninstall
	cp script.sh chrome.sh ${BIN}
	ln -s script.sh ${BIN}/run
	ln -s chrome.sh ${BIN}/chrome