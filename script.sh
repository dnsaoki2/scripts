#!/bin/bash

## disponibiliza todos os comando dentro das pastas configuradas pela variavel PROJECT_PATHS


# PROJECT_PATH="/Users/denis.aoki/workspace/personal/scripts/structure"
PROJECT_PATHS=$(echo $PROJECT_PATHS | tr ";" "\n")

function available_commands {
  echo "Uso: "
  echo "  run <command> <...options>"
  echo ""
  echo "command:"
  for path in $PROJECT_PATHS
  do
    for file in $(find $path -type f \( -iname "*.py" ! -name "*base*.py" \))
    do
      command_name=$(echo $file | rev | cut -d '/' -f1 | rev | cut -d '.' -f1)
      echo -e "  - $command_name"
    done
  done
}


function verify {
  check="false"
  for path in $PROJECT_PATHS
  do
    if [ $(find $path -type f -iname "$1.py") ]
    then
      cd $path
      check="true"
      python "$1.py" ${@:2}
      exit 0
    fi
  done
  if [ $check == "false" ]
  then
    echo "Comando inválido"
    echo ""
    available_commands
    exit 1
  fi
}

case $1 in
"man") available_commands ;;
*) verify $@
esac